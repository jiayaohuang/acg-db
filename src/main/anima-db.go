package main

import (
	"net/http"
	"log"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
)

var (
	dborm orm.Ormer
	cond *orm.Condition
)

func init() {
	orm.RegisterModel(new(AnimaInfo))
	orm.RegisterModel(new(ComicInfo))
	orm.RegisterModel(new(GameInfo))
	orm.RegisterModel(new(NovelInfo))

	orm.RegisterDataBase("default", "sqlite3", "anima-db.db")
	orm.RunSyncdb("default", false, false)
	cond = orm.NewCondition()
}

func main() {
	dborm = orm.NewOrm()

	http.HandleFunc("/", handleRoot)
	http.HandleFunc("/search", handleSearch)
	http.HandleFunc("/anima/", handleAnima)
	http.HandleFunc("/anima-edit/", handleAnimaEdit)
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	log.Println("Start server")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Println("ListenAndServe: ", err)
		return
	}
}
