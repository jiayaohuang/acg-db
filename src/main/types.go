package main

type BaseInfo struct {
	Id int `orm:"pk;auto"`
	Name string `orm:"not null;unique"`
	JpName string `orm:"null"`
	OthName string `orm:"null"`
	Series string `orm:"not null"`
	Category string `orm:"not null"`
	Auther string `orm:"null"`
}

type AnimaInfo struct {
	BaseInfo
	Director string `orm:"null"`
	Product string `orm:"null"`
	ShowTime string `orm:"not null"`
	OthSeasons string `orm:"null"`
	OrigClass string `orm:"null"`
	Characters string `orm:"not null"`
	TextNote string `orm:"null;size(524288)"`
	TextRole string `orm:"null;size(524288)"`
	Synopsis string `orm:"null;size(524288)"`
	RelatedLinks string `orm:"null; size(2048)"`
}

type ComicInfo struct {
	BaseInfo
}

type GameInfo struct {
	BaseInfo
}

type NovelInfo struct {
	BaseInfo
}

