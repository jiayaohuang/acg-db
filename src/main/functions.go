package main

import (
	"strings"
)

func sliceToString(sli []string) string {
	var (
		str string
	)
	n := len(sli)
	for i, v := range sli {
		str += v
		if i >= n - 1{
			break
		}
		str += ";"
	}
	return str
}

func stringToSlice(str string) []string {
	var (
		sli []string
	)
	sli = strings.Split(str, ";")
	return sli
}

