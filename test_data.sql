/*
BEGIN TRANSACTION;
CREATE TABLE `novel_info` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(255) NOT NULL DEFAULT '' ,
    `jp_name` varchar(255),
    `oth_name` varchar(255)
, `category` varchar(255) NOT NULL  DEFAULT '');
CREATE TABLE `game_info` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(255) NOT NULL DEFAULT '' ,
    `jp_name` varchar(255),
    `oth_name` varchar(255)
, `category` varchar(255) NOT NULL  DEFAULT '');
CREATE TABLE `comic_info` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(255) NOT NULL DEFAULT '' ,
    `jp_name` varchar(255),
    `oth_name` varchar(255)
, `category` varchar(255) NOT NULL  DEFAULT '');
CREATE TABLE `anima_info` (
    `id` integer NOT NULL PRIMARY KEY,
    `name` varchar(255) NOT NULL DEFAULT '' ,
    `jp_name` varchar(255),
    `oth_name` varchar(255),
    `main_role` varchar(255) NOT NULL DEFAULT '' ,
    `auther` varchar(255),
    `director` varchar(255),
    `product` varchar(255),
    `show_time` varchar(255) NOT NULL DEFAULT '' ,
    `orig_class` varchar(255),
    `text_role` varchar(524288),
    `text_note` varchar(524288)
, `oth_seasons` varchar(255), `related_links` varchar(2048), `category` varchar(255) NOT NULL  DEFAULT '');
*/
INSERT INTO `anima_info` (id,name,jp_name,oth_name,main_role,auther,director,product,show_time,orig_class,text_role,text_note,oth_seasons,related_links,category) VALUES (1,'野良神','ノラガミ','Noragami；流浪神差','夜斗;一岐日和;雪音;野良','安达渡嘉 ','田村耕太郎','BONES','2014-01','漫画','夜斗 	神谷浩史 	居无定所的落魄神明，专门帮助人解决事件。
一岐日和 	内田真礼 	普通的初中女生，因故很容易灵魂出窍。
雪音 	梶裕贵 	夜斗的神器，原来是一个14岁的死灵。
野良 	钉宫理惠 	夜斗的前神器，身着寿衣，头戴天冠。
毘沙门 	泽城美雪 	有着“最强武神”之称的七福神之一。
兆麻 	福山润 	毘沙门的神器,同时也是她的“道司”。
小福 	丰崎爱生 	真实身份是带来坏运与不幸的穷神。[8] 
大黑 	小野大辅 	小福的神器。
天神 	大川透 	真实身份是菅原道真，是掌管学问与知识的神祇。
梅雨 	早见沙织 	自愿在天神身边侍奉的梅木精,并非其神器。
真喻 	今井麻美 	原来是夜斗的神器“伴音”,后来辞职成为天神的神器,名“真”。
歩喩 	市道真央 	天神的神器,“天神Sisters”的一员。
南喩 	茜屋日海夏 	天神的神器,“天神Sisters”的一员。
实喻 	タカオユキ 	天神的神器,“天神Sisters”的一员。
望喻 	井泽诗织 	天神的神器,“天神Sisters”的一员。
囷巴 	井上和彦 	毘沙门的神器。
纴巴 	佐藤奏美 	毘沙门的神器，长发的女性。
山下晶 	高山优子 	壹岐日和的同班同学。
田端爱美 	高桥未奈美 	壹岐日和的同班同学。
睦实 	小松未可子 	因被同学欺负和无视而向夜斗寻求帮助。
浦泽裕介 	小野友树 	喜欢小福，后切断与小福的缘分。 ','电视动画《野良神》改编自安达渡嘉创作的同名神幻题材漫画作品，制作公司由骨头社（BONES）担当。“野良”在日语中原指旷原、田野，也可形容居无定所的流浪者。故事讲述了居无定所、没有工作、自称是“神”的穿着运动衫的夜斗与普通的初三女生壹岐日和因为一次意外的事故而结缘，并由此展开的一系列奇幻冒险故事。
作品于2014年1月5日开始陆续在每日放送（MBS）、TOKYO MX、爱知电视台、BS11等日本各大电视台播出，首播时间为TOKYO MX的每周日深夜23：30（北京时间22:30）。动画在中国大陆的网络播放权由爱奇艺取得，更新时间为稍迟于日方最早一档电视版播放完毕后的每周日晚23:00[1-2]  ，并于2014年3月23日播放完毕，共12集，已完结。
2015年4月6日发售的《月刊少年Magazine》2015年5月号上发表了动画第2期制作决定的消息[3]  。第2期动画《野良神 ARAGOTO》于2015年秋开始播出。
','野良神 ARAGOTO',NULL,'anima');
INSERT INTO `anima_info` (id,name,jp_name,oth_name,main_role,auther,director,product,show_time,orig_class,text_role,text_note,oth_seasons,related_links,category) VALUES (2,'东京暗鸦','東京レイヴンズ','东京乌鸦','土御门春虎;土御门夏目;阿刀冬儿;北斗','字野耕平','金崎贵臣','8 BIT','2013-10','轻小说','土御门春虎	石川界人	
木暮禅次朗	高桥伸也	
土御门夏目	花泽香菜	
镜伶路	吉野裕行	
阿刀冬儿	木村良平	
弓削麻里	井上麻里奈	
北斗	金元寿子	
三善十悟	土田大	
大连寺铃鹿	佐仓绫音	
土御门夜光	置鲇龙太郎	
仓桥京子	喜多村英梨	
土御门泰纯	速水奖	
百枝天马	下野纮	
土御门鹰宽	志村知幸	
空/飞车丸	丰崎爱生	
土御门千鹤	渡边明乃	
角行鬼	安元洋贵	
相马多轨子	金元寿子	
转生前的飞车丸	甲斐田裕子	
比良多笃祢	石田彰	
仓桥美代	平野文	
六人部千寻	诹访部顺一	
大友阵	游佐浩二	
牧原义隆	最上嗣生	
藤原	千田光男	
江藤	佐藤健辅	
富士野真子	三石琴乃	
芦屋道满	飞田展男	
木府亚子	久川绫	
早乙女凉	橘田泉	
仓桥源司	小杉十郎太	
大连寺利矢	矢岛晶子	
天海大善	石丸博也	
楔拔	寺岛拓笃	
宫地岩夫	石冢运升','电视动画《东京暗鸦》改编自日本轻小说家字野耕平原作的同名轻小说。2012年10月19日，在富士见书房首页上公开了《东京暗鸦》动画化企划进行中的消息[1]  。电视动画于2013年10月8日开始播放，全24集。
过去大战时期，日本帝国陆军为将咒术运用在军事上而建立阴阳寮，并派任传说中阴阳师安倍晴明的後裔、土御门家当家的天才阴阳师土御门夜光担任统领，成功缔造日本咒术界的改革，成为现代咒术之父。
大战末期，处于败势的帝国陆军要求夜光实行大规模咒术仪式以挽回局势，但结果仪式失败，不只做为执行者的夜光身亡，还强烈的扰乱首都东京的灵气，使东京百鬼夜行、妖魅纵横。直到了战后阴阳寮改名阴阳厅，负责处理日本各地－主要是东京－发生的灵力灾害，而阴阳师也因而活跃在现代的舞台之上。
土御门春虎是个出生土御门家的少年，虽身为阴阳师名门后代，却毫无阴阳师才能，过着一般人的生活。某年夏天，春虎遇见了他的童年玩伴，也是土御门家的下任当家的少女夏目。过去曾允下承诺如今却分道而行的两人，在此刻相见之后，土御门的历史开始转动了起来......',NULL,NULL,'anima');
INSERT INTO `anima_info` (id,name,jp_name,oth_name,main_role,auther,director,product,show_time,orig_class,text_role,text_note,oth_seasons,related_links,category) VALUES (3,'我们大家的河合庄','僕らはみんな河合荘',NULL,'宇佐和成;河合律;锦野麻弓;城崎;渡边彩花;住子','宫原琉璃','宫繁之','Brain''s Base','2014-04','漫画','宇佐和成：井口佑一
河合律：花泽香菜
锦野麻弓：佐藤利奈
城崎：四宫豪
渡边彩花：金元寿子
住子：小林沙
林：沼仓爱美
北条：绿川光
西园寺：近藤隆
常田美晴：诹访彩花','电视动画《我们大家的河合庄》改编自日本漫画家宫原琉璃原作的同名漫画。在少年画报社旗下的漫画杂志《Young King OURs》2014年1月号（2013年11月30日发售）上发表了动画化的消息[1]  。电视动画于2014年4月3日起开始播放，全13话，其中TV未放送的第13话收录在BD&DVD第7卷中。
故事讲述了因为父母工作迁移而开始独居的男子高中生宇佐入住进了河合庄，在那里，他发现自己暗恋的前辈小律居然也和自己住在同一个地方。于是，宇佐便希望能够在升入高中以后安稳、充实地与聪明又清秀的她共度美好时光，只可惜在初中时代总遇到倒霉事的他，如今又碰见了一个变态级的舍友……',NULL,NULL,'anima');
COMMIT;
