<!DOCTYPE html>
<html>
	<head>
		<meta content="text/html; charset=utf-8">
		<title>{{.Title}}</title>
		<link rel="stylesheet" type="text/css" href="/static/css/pages.css" />
		<!-- https://github.com/chjj/marked -->
		<script src="/static/js/marked.js" type="text/javascript"></script>
	</head>
	<body class="content">
		<h1>{{.Title}}</h1>
		<div class="sidenote">
			<table border=1>
				<tr><td>作品名称</td><td>{{.Info.Name}}</td></tr>
				<tr><td>日文原名</td><td>{{.Info.JpName}}</td></tr>
				<tr><td>其它译名</td><td>{{.Info.OthName}}</td></tr>
				<tr><td>所属系列</td><td>{{.Info.Series}}</td></tr>
				<tr><td>作品形式</td><td>{{.Info.Category}}</td></tr>
				<tr><td>原著作者</td><td>{{.Info.Auther}}</td></tr>
				<tr><td>导演</td><td>{{.Info.Director}}</td></tr>
				<tr><td>制作商</td><td>{{.Info.Product}}</td></tr>
				<tr><td>放送时间</td><td>{{.Info.ShowTime}}</td></tr>
				<tr><td>其它季度</td><td>
					{{range .OthSeasons}}
						<a href="/anima/{{.}}">{{.}}</a>;
					{{end}}
				</td></tr>
				<tr><td>原著载体</td><td>{{.Info.OrigClass}}</td></tr>
				<tr><td>主角</td><td>
					{{ $Series := .Info.Series }}
					{{range .Characters}}
						<a href="/character/{{$Series}}/{{.}}">{{.}}</a>;
					{{end}}
				</td></tr>
			</table>
		</div>

		<h2>作品简介:</h2>
		<div><pre id="view_note" class="mdarea md-view"></pre></div>
		<h2>角色配音:</h2>
		<div id="view_role" class="mdarea md-view"></div>
		<h2>分集剧情:</h2>
		<div id="view_synopsis" class="mdarea md-view"></div>
		<h2>相关链接:</h2>
		<div id="view_related_links" class="mdarea md-view"></div>

		<form action="/anima/{{.Info.Name}}" method="post" name="anima_delete">
			<input type="hidden" name="name" value="{{.Info.Name}}">
			<input type="hidden" name="method" value="delete">
		</form>

		<div align="right">
			<a href="javascript:deleteThis();"
				class="btn btn-red">删除</a>
			<a href="/anima-edit/{{.Info.Name}}"
				class="btn btn-cyan">编辑</a>
		</div>

		<script type="text/javascript">
			function deleteThis() {
				if (confirm("您确认要删除该条词条吗？")){
					document.anima_delete.submit();
				}
			}
			document.getElementById("view_note").innerHTML =
				marked("{{.Info.TextNote}}");
			document.getElementById("view_role").innerHTML =
				marked("{{.Info.TextRole}}");
			document.getElementById("view_synopsis").innerHTML =
				marked("{{.Info.Synopsis}}");
			document.getElementById("view_related_links").innerHTML =
				marked("{{.Info.RelatedLinks}}");
		</script>
	</body>
</html>

