<!DOCTYPE html>
<html>
	<head>
		<meta content="text/html; charset=utf-8">
		<title>UPDATE SUCCESS: {{.Title}}</title>
		<link rel="stylesheet" type="text/css" href="/static/css/pages.css" />
	</head>
	<body class="content">
		<h2>UPDATE SUCCESS: {{.Title}}</h2>
		<center>
			<a class="btn btn-cyan" href="/{{.Info.Category}}/{{.Info.Name}}">返回词条</a>
		</center>
	</body>
</html>

