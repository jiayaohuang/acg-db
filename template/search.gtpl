<!DOCTYPE html>
<html>
	<head>
		<meta content="text/html; charset=utf-8">
		<title>Search</title>
		<link rel="stylesheet" type="text/css" href="/static/css/pages.css" />
	</head>
	<body class="content">
		<h2>Search</h2>
		<form action="/search" method="post" name="search">
			Keyword:<input type="text" name="keyword" value="{{.Keyword}}">
			</br>
			Category:
			<input type="checkbox" name="category" value="anima"
				checked="checked">animation
			<input type="checkbox" name="category" value="comic"
				checked="checked">comic
			<input type="checkbox" name="category" value="game"
				checked="checked">game
			<input type="checkbox" name="category" value="novel"
				checked="checked">novel
			</br>
			<button type="submit" class="btn btn-cyan btn-fix">按名称搜索</button>
		</form>
		<h3>Search result</h3>
		{{with .Anima}}
			{{range .}}
			<p>
				<a href="/anima/{{.Name}}">{{.Name}}</a>
				( Category: {{.Category}} )
			</p>
			{{end}}
		{{end}}
		{{with .Comic}}
			{{range .}}
			<p>
				<a href="/anima/{{.Name}}">{{.Name}}</a>
				( Category: {{.Category}} )
			</p>
			{{end}}
		{{end}}
		{{with .Game}}
			{{range .}}
			<p>
				<a href="/anima/{{.Name}}">{{.Name}}</a>
				( Category: {{.Category}} )
			</p>
			{{end}}
		{{end}}
		{{with .Novel}}
			{{range .}}
			<p>
				<a href="/anima/{{.Name}}">{{.Name}}</a>
				( Category: {{.Category}} )
			</p>
			{{end}}
		{{end}}
	</body>
</html>
