<!DOCTYPE html>
<html>
	<head>
		<meta content="text/html; charset=utf-8">
		<title>{{.Title}}</title>
		<link rel="stylesheet" type="text/css" href="/static/css/pages.css" />
		<!-- https://github.com/chjj/marked -->
		<script src="/static/js/marked.js" type="text/javascript"></script>
		<script src="/static/js/md-preview.js" type="text/javascript"></script>
	</head>
	<body class="content">
		<h1>{{.Title}}</h1>
		<form action="/anima-edit/{{.Info.Name}}" method="post" name="anima_edit">
			<input name="id" type="hidden" value="{{.Info.Id}}">
			<div class="edit-form-item"><label>作品名称</label>
				<input name="name" type="text" readonly value="{{.Info.Name}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>日文原名</label>
				<input name="jp_name" type="text" value="{{.Info.JpName}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>其它译名</label>
				<input name="oth_name" type="text" value="{{.Info.OthName}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>所属系列</label>
				<input name="series" type="text" value="{{.Info.Series}}"
					required="required"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>作品形式</label>
				<input name="category" type="text" readonly value="{{.Info.Category}}"
					required="required"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>原著作者</label>
				<input name="auther" type="text" value="{{.Info.Auther}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>导演</label>
				<input name="director" type="text" value="{{.Info.Director}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>制作商</label>
				<input name="product" type="text" value="{{.Info.Product}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>放送时间</label>
				<input name="show_time" type="text" value="{{.Info.ShowTime}}"
					required="required"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>其它季度</label>
				<input name="oth_seasons" type="text" value="{{.Info.OthSeasons}}"></input>
				</br>
			</div>
			<div class="edit-form-item"><label>原著载体</label>
				<select id="orig_class" name="orig_class" required="required">
					<option value="anima">动画</option>
					<option value="comic">漫画</option>
					<option value="novel">小说</option>
					<option value="game">游戏</option>
				</select>
				<!-- <input name="orig_class" type="text" value="{{.Info.OrigClass}}"></input> -->
				</br>
			</div>
			<div class="edit-form-item"><label>主角</label>
				<input name="characters" type="text" value="{{.Info.Characters}}"
					required="required"></input>
				</br>
			</div>

				<h2>作品简介:</h2>
				<div id="view_note" class="mdarea md-pre"></div>
				<textarea wrap="virtual" class="mdarea md-edit" oninput="this.editor.update()"
					name="text_note" id="edit_note">{{.Info.TextNote}}</textarea>
				<h2>角色配音:</h2>
				<div id="view_role" class="mdarea md-pre"></div>
				<textarea wrap="virtual" class="mdarea md-edit" oninput="this.editor.update()"
					name="text_role" id="edit_role">{{.Info.TextRole}}</textarea>
				<h2>分集剧情:</h2>
				<div id="view_synopsis" class="mdarea md-pre"></div>
				<textarea wrap="virtual" class="mdarea md-edit" oninput="this.editor.update()"
					name="synopsis" id="edit_synopsis">{{.Info.Synopsis}}</textarea>
				<h2>相关链接:</h2>
				<div id="view_related_links" class="mdarea md-pre"></div>
				<textarea wrap="virtual" class="mdarea md-edit" oninput="this.editor.update()"
					name="related_links" id="edit_related_links">{{.Info.RelatedLinks}}</textarea>

			<div align="right">
				<a href="/anima/{{.Info.Name}}"
					class="btn btn-red">放弃</a>
				<button type="submit" class="btn btn-cyan btn-fix">保存</button>
			</div>
		</form>
	</body>

	<script type="text/javascript">
		document.getElementById("orig_class").value="{{.Info.OrigClass}}";
		new Editor($("edit_note"), $("view_note"));
		new Editor($("edit_role"), $("view_role"));
		new Editor($("edit_synopsis"), $("view_synopsis"));
		new Editor($("edit_related_links"), $("view_related_links"));
	</script>

</html>

