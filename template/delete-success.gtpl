<!DOCTYPE html>
<html>
	<head>
		<meta content="text/html; charset=utf-8">
		<title>DELETE SUCCESS: {{.Name}}</title>
		<link rel="stylesheet" type="text/css" href="/static/css/pages.css" />
	</head>
	<body class="content">
		<h2>DELETE SUCCESS: {{.Title}}</h2>
		<center>
			<a class="btn btn-cyan" href="/{{.Info.Category}}">返回上一级</a>
		</center>
	</body>
</html>

